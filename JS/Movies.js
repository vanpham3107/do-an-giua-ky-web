const api = "https://api.themoviedb.org/3/";
const api_key = "api_key=2c71a06526de1afab7b9b9994027f1ff";
const images = "https://image.tmdb.org/t/p/w500";
const movie = `https://api.themoviedb.org/3/search/movie?${api_key}`;
const person = `https://api.themoviedb.org/3/search/person?${api_key}`;

async function loadpage() {
  const req = `${api}movie/upcoming?${api_key}&page=1`;

  loading();

  const responese = await fetch(req);
  const rs = await responese.json();

  listMovies(rs.results);
}

function loading() {
  $("#main").empty();
  $("#main").append(`
        <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    `);
}

function listMovies(ms) {
  $("#main").empty();
  for (const m of ms) {
    const poster = images.concat(m.poster_path);
    $("#main").append(`
            <div class="col-md-4 py-1">
                <div class="card shadow h-100" onclick="loadMovie('${m.id}')">
                    <img class="card-img-top" src="${poster}" alt="${m.title}" >
                    <div class="card-body">
                        <h5 class="card-title">${m.title}</h5>
                        <h5 class="card-title">Rate: 
                            <small>${m.vote_average}(${m.vote_count})</small></h5>
                        <h5 class="card-title">Release_date:<small> ${m.release_date}</small></h5>
                        <h5 class="card-title">Popularity: <small>${m.popularity}</small></h5>
                        <h5 class="my-4">Description:<br/>
                            <small>${m.overview}</small>
                        </h5>   
                    </div>
                </div>
            </div>
        `);
  }
}

async function search(e) {
  e.preventDefault();

  const strSearch = $("form input").val();
  const reqstr_movie = `${api}search/movie?${api_key}&query=${strSearch}`;
  const reqstr_star = person.concat("&query=", strSearch);

  loading();

  const responese_movie = await fetch(reqstr_movie);
  const rs_movie = await responese_movie.json();

  listMovies(rs_movie.results);

  const responese_star = await fetch(reqstr_star);
  const rs_star = await responese_star.json();

  listStar(rs_star.results);
}

async function listStar(ms) {
  for (const m of ms) {
    const id_star = `https://api.themoviedb.org/3/person/${m.id}/movie_credits?api_key=2c71a06526de1afab7b9b9994027f1ff`;

    const responese = await fetch(id_star);
    const rs = await responese.json();
    for (const n of rs.cast) {
      const poster = images.concat(n.poster_path);
      $("#main").append(`
                <div class="col-md-4 py-1">
                    <div class="card shadow h-100" onclick="detailMovie('${n.id}')">
                        <img class="card-img-top" src="${poster}" alt="${n.title}">
                        <div class="card-body">
                            <h5 class="card-title">${n.title}</h5>
                            <h5 class="card-title">vote_average: ${n.vote_average}</h5>
                            <p class="card-text">${n.overview}</p>
                            <h5 class="card-title">release_date: ${n.release_date}</h5>
                        </div>
                    </div>
                </div>
            `);
    }
  }
}
async function loadMovie(id) {
  const reqStr = `${api}movie/${id}?${api_key}`;
  const reqCd = `${api}movie/${id}/credits?${api_key}`;
  const reqRv = `${api}movie/${id}/reviews?${api_key}`;

  loading();

  const response = await fetch(reqStr);
  const movie = await response.json();

  const respCC = await fetch(reqCd);
  const castcrew = await respCC.json();

  const respRv = await fetch(reqRv);
  const review = await respRv.json();

  MovieDetail(movie, castcrew, review);
}

function MovieDetail(m, cc, rv) {
  const image = images.concat(m.poster_path);
  $("#main").empty();
  $("#main").append(`
        <div class="container">
            <div class="row shadow">
                <div class="col-md-4">
                    <img class="img-fluid" src="${image}" alt="${m.title}">
                </div>
                <div class="col-md-8">
                <h3 class="my-4">${m.original_title}
                    <small>(${m.release_date})</small>
                    </h3>
                    <h4 class="my-4">Rate:
                    <small>${m.vote_average}(${m.vote_count})</small>
                    </h4>
                    <h4 class="my-4"> Genres:
                    <p id="genres"></p>
                    </h4>
                    <h4 class="my-4">Time:
                    <small>${m.runtime}</small>
                    </h4>
                    <h4 class="my-4">Overview: <br/>
                    <small>${m.overview}</small>
                    </div>
                </h4>
            </div>
        <div id="person-movie"></div>    
        </div>
    `);
  $("#genres").empty();
  for (const gr of $(m.genres)) {
    $("#genres").append(`
            <small>${gr.name}<br></small>
        `);
  }
  personMovie(cc, rv);
}

function personMovie(cc, rv) {
  $("#main #person-movie").empty();
  $("#main #person-movie").append(`
    <div class="row shadow"> 
    <div class="col-md-12 shadow">
        <h4 class="my-4">Director:
            <div class="container shadow">
                 <div id="director" class="row py-1" ></div>
            </div>
        </h4>
        <h4 class="my-4">Crew:
            <div class="container shadow">
                 <div id="crew" class="row py-1" ></div>
            </div>
        </h4>
        <h4 class="my-4">Cast:
            <div class="container shadow">
                <div id="cast" class="row py-1" >
                </div>
            </div>
        </h4>
        <h4 class="my-4">Review:
            <div class="container shadow">
                <div id="review" class="row py-1" >
                </div>
            </div>
        </h4>
    </div>
    `);
  CastCrew(cc);
  Review(rv);
}

function CastCrew(ccs) {
  $("#cast").empty();
  for (const cs of $(ccs.cast)) {
    $("#cast").append(`
            <div class="col-md-3 py-1">
                <div class="card shadow h-100" onclick="loadPerson('${cs.id}')">
                    <img src="https://image.tmdb.org/t/p/w500${cs.profile_path}" class="card-img-top" alt="${cs.name}">
                    <div class="card-body">
                    <h5 class="my-4">${cs.name}<br/>
                        <small>(${cs.character})</small>
                    </h5>
                    </div>
                </div>
            </div>
        `);
  }
  $("#director").empty();
  for (const drt of $(ccs.crew)) {
    if (`${drt.job}` === "Director") {
      $("#director").append(`
                <div class="col-md-3 py-1">
                    <div class="card shadow h-100">
                        <img src="https://image.tmdb.org/t/p/w500${drt.profile_path}" class="card-img-top" alt="${drt.name}">
                        <div class="card-body">
                        <h5 class="my-4">${drt.name}<br/>
                            <small>(${drt.job})</small>
                        </h5>
                        </div>
                    </div>
                </div>
            `);
    }
  }
  $("#crew").empty();
  for (const crs of $(ccs.crew)) {
    if (`${crs.job}` !== "Director") {
      $("#crew").append(`
                <div class="col-md-3 py-1">
                    <div class="card shadow h-100">
                        <img src="https://image.tmdb.org/t/p/w500${crs.profile_path}" class="card-img-top" alt="${crs.name}">
                        <div class="card-body">
                        <h5 class="my-4">${crs.name}<br/>
                            <small>(${crs.job})</small>
                        </h5>
                        </div>
                    </div>
                </div>
            `);
    }
  }
}

function Review(rvs) {
  $("#review").empty();
  for (const rv of $(rvs.results)) {
    $("#review").append(`
            <div class="card-body">
                <h5 class="my-4">${rv.author}
                    <small>(${rv.url})<br/>
                    ${rv.content}</small>
                </h5>
            </div>
        `);
  }
}


async function loadPerson(id)
{
    const reqPs = `${api}person/${id}?${api_key}`;
    const reqMvPs = `${api}person/${id}/movie_credits?${api_key}`;

    loading();

    const respPs = await fetch(reqPs);
    const person = await respPs.json();

    const respMvPs = await fetch(reqMvPs);
    const personMv = await respMvPs.json();

    PersonDetail(person, personMv);
}

function PersonDetail(ps,psmv){
    $('#main').empty();
    $('#main').append(`
        <div class="container">
            <div class="row shadow">
                <div class="col-md-4">
                    <img src="https://image.tmdb.org/t/p/w500${ps.profile_path}" class="card-img-top" alt="${ps.name}">
                </div>
                <div class="col-md-8">
                    <h3 class="my-4">${ps.name}</h3><br/>
                    <h4 class="my-4">Biography: <br/>
                        <small>${ps.biography}</small>
                    </h4>
                </div>
            </div>
            <div id="list_movie" class="row py-1" ></div>
        </div>
    `);
    MoviePerson(psmv);
}

function MoviePerson(mvps)
{
    $('#list_movie').empty();
    for(const mv of mvps.cast){
        $('#list_movie').append(`
            <div class="col-md-4 py-1">
                <div class="card shadow h-100" onclick="loadDetail('${mv.id}');">
                <img src="https://image.tmdb.org/t/p/w500${mv.poster_path}" class="card-img-top" alt="${mv.original_title}">
                    <div class="card-body">
                        <h4 class="card-title">${mv.original_title}</h4>
                        <h5 class="my-4">Rate:
                            <small>${mv.vote_average}(${mv.vote_count})</small>
                        </h5>
                        <h5 class="my-4">Release_data:
                            <small>${mv.release_date}</small>
                        </h5>
                        <h5 class="my-4">Popularity:
                        <small>${mv.popularity}</small>
                        </h5>
                        <h5 class="my-4">Description:<br/>
                            <small>${mv.overview}</small>
                        </h5>
                    </div>
                </div>
            </div>
        `);
    }
}

